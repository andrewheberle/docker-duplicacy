FROM registry.gitlab.com/andrewheberle/docker-remco:alpine

ARG DUPLICACY_VERSION=2.7.2

RUN apk --no-cache add curl ca-certificates tzdata && \
    curl -L https://github.com/gilbertchen/duplicacy/releases/download/v${DUPLICACY_VERSION}/duplicacy_linux_x64_${DUPLICACY_VERSION} > /usr/local/bin/duplicacy && \
    chmod +x /usr/local/bin/duplicacy && \
    apk --no-cache del curl

COPY rootfs /

ENV DUPLICACY_THREADS="4" \
    DUPLICACY_KEY="" \
    DUPLICACY_KEY_PASSPHRASE="" \
    DUPLICACY_SNAPSHOT="snapshot-name" \
    DUPLICACY_REPOSITORY="/data" \
    DUPLICACY_BACKUP="disabled" \
    DUPLICACY_BACKUPCOPY="disabled" \
    DUPLICACY_COPY="disabled" \
    DUPLICACY_BACKUP_SCHEDULE="0 20 * * *" \
    DUPLICACY_BACKUPCOPY_SCHEDULE="0 20 * * *" \
    DUPLICACY_BACKUP_STORAGE="default" \
    DUPLICACY_BACKUP_LIMIT="0" \
    DUPLICACY_COPY_STORAGE="remote" \
    DUPLICACY_COPY_SCHEDULE="0 2 * * *" \
    DUPLICACY_COPY_LIMIT="0" \
    DUPLICACY_WEBHOOK="" \
    DUPLICACY_WEBHOOK_END=""