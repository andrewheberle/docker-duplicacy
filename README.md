# Duplicacy Container

This is a minimal container used to run scheduled backups and copies of a repository.

It is assumed that your repository and storage(s) are already configured ready to go.

## Usage

Running under Docker:

```sh
docker run -d --name duplicacy \
    -v /path/to/repository:/data \
    -e DUPLICACY_BACKUP=enabled \
    registry.gitlab.com/andrewheberle/docker-duplicacy:2.7.2-1
```

The above will run a backup on the default schedule (daily at 8pm) to the default storage.

Under Kubernetes:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: duplicacy
  namespace: duplicacy
spec:
  selector:
    matchLabels:
      app: duplicacy
  template:
    metadata:
      labels:
        app: duplicacy
    spec:
      containers:
      - name: duplicacy
        image: registry.gitlab.com/andrewheberle/docker-duplicacy:2.7.2-1
        imagePullPolicy: IfNotPresent
        env:
        - name: DUPLICACY_BACKUP
          value: enabled
        volumeMounts:
        - mountPath: /data
          name: duplicacy-repository
      volumes:
      - name: duplicacy-repository
        nfs:
          path: /path/to/repository
          server: 1.2.3.4
```

## Configuration

Configuration of the container is all via environment variables as follows:

| Variable                      | Default    | Usage                                     |
|-------------------------------|------------|-------------------------------------------|
| DUPLICACY_THREADS             | 4          | Set the number of upload/download threads |
| DUPLICACY_KEY                 | No Default | RSA key for copy destination              |
| DUPLICACY_KEY_PASSPHRASE      | No Default | Passphrase for RSA key                    |
| DUPLICACY_SNAPSHOT            | No Default | Snapshot ID to copy (rather than all)     |
| DUPLICACY_REPOSITORY          | /data      | Location of repository. Mount this!       |
| DUPLICACY_BACKUP              | disabled   | Enable backup                             |
| DUPLICACY_BACKUP_SCHEDULE     | 0 20 * * * | Backup schedule (crontab format)          |
| DUPLICACY_BACKUP_STORAGE      | default    | Storage to save backup to                 |
| DUPLICACY_BACKUP_LIMIT        | 0          | Transfer limit for backup (in Kb/sec)     |
| DUPLICACY_COPY_STORAGE        | remote     | Storage to copy backups to                |
| DUPLICACY_BACKUPCOPY          | disabled   | Enable backup with an immediate copy      |
| DUPLICACY_BACKUPCOPY_SCHEDULE | 0 20 * * * | Backup schedule (crontab format)          |
| DUPLICACY_COPY                | disabled   | Enable copy                               |
| DUPLICACY_COPY_SCHEDULE       | 0 2 * * *  | Copy schedule (crontab format)            |
| DUPLICACY_COPY_LIMIT          | 0          | Transfer limit for copy (in Kb/sec)       |
